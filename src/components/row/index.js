import React, { Component } from 'react';
import AccessAlarmIcon from '@material-ui/icons/AccessAlarm';
import ThreeDRotation from '@material-ui/icons/ThreeDRotation';
import EditIcon from '@material-ui/icons/Edit';
import SaveAltIcon from '@material-ui/icons/SaveAlt';


export default class Row extends Component {
    constructor(props) { 
        super(props)
        this.state = {
            value: props.rowData.rate
        }
        this.setEditMode = this.setEditMode.bind(this); //прив'язуємо this
        this.changeValueHandler = this.changeValueHandler.bind(this);
        this.saveValue = this.saveValue.bind(this);
    }

    setEditMode() {
        this.setState({
            ...this.state,
            isEditMode: true
        })
    }

    changeValueHandler(event) {
        this.setState({
            ...this.state, //відбувається копіювання всіх властивостей, а не посилання на масив, оператор spread
            value: event.target.value
        })
    }

    saveValue() {
        this.props.rowData.rate = parseFloat(this.state.value);// строку перетворюємо в число
        this.setState({
            ...this.state, //
            isEditMode: false
        })
    }

    render() {
        const { rowData: item } = this.props;
        return (
            <tr>
                <td>{item.txt}</td>
                <td>
                    <div style={{ display: this.state.isEditMode ? 'block' : 'none' }}><input value={this.state.value} onChange={this.changeValueHandler}></input></div>
                    <div style={{ display: !this.state.isEditMode ? 'block' : 'none' }}>{item.rate ? item.rate.toFixed(2) : item.value}</div>
                </td>
                <td>{item.cc}</td>
                <td>
                    <EditIcon style={{ display: !this.state.isEditMode ? 'block' : 'none' }} onClick={this.setEditMode}>Edit</EditIcon>
                    <SaveAltIcon style={{ display: this.state.isEditMode ? 'block' : 'none' }} onClick={this.saveValue}>Save</SaveAltIcon>
                </td>
            </tr>
        )
    }
}