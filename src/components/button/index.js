import React from 'react';

import './button.css';

export default function Button({ value }) {
    return (
        <>
            <div><input type='button' value={value} className='button' /></div>
        </>
    )
}