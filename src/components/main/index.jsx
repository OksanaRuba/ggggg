import React, { Component } from 'react';
import Button from '../button';
import Section from '../section';
import { BrowserRouter as Router, Switch, Route, NavLink } from "react-router-dom";
import Home from '../home';
import Error from '../error';

import './main.css';

class Main extends Component {
    render() { // у методі записуємо return
        return (
            <>
                <Router>
                    <header>
                        <NavLink to='/currency'><Button value='Офіційний курс гривні до іноземних валют та облікова ціна банківських металів'></Button></NavLink>
                        <NavLink to='/home' ><Button value='Home'></Button></NavLink>
                        <NavLink to='/bank-info'><Button value='Основні показники діяльності банків України'></Button></NavLink>
                    </header>
                    <Switch>
                        <Route exact path='/currency'>
                            <Section url='https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'></Section>
                        </Route>
                        <Route exact path='/bank-info'>
                            <Section url='https://bank.gov.ua/NBUStatService/v1/statdirectory/basindbank?date=20160101&period=m&json'></Section>
                        </Route>
                        <Route exact path='/home' component={Home} />
                        <Route component={Error} />
                    </Switch>
                </Router>
            </>
        )
    }
}
/*
class Main extends Component {
    render() {
        // перший спосіб створення запиту
        /*let promise = fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
        promise.then((data) => {return data.json()})
        .then((data1)=> {console.log(data1)})
          
    return (
        <>
            <Router>
                <header>
                    <Button value='1'></Button>
                    <Button value='2'></Button>
                    <Button value='3'></Button>
                </header>
                    <Switch>
                        <Route exact path='/currency' component={Button}>
                          <Section url='https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'></Section>
                        </Route>
                    </Switch>
                    <Switch>
                        <Route exact path='/bank-info' component={Button}>
                            <Section url='https://bank.gov.ua/NBUStatService/v1/statdirectory/basindbank?date=20160101&period=m&json'></Section>
                        </Route>
                        <Router component={Error} />
                    </Switch>
                </Router>
            </>
        )
    }

}
 
*/
export default Main;