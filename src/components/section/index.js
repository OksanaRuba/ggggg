import React, { Component } from 'react';
import { data } from '../request';
import LinearProgress from '@material-ui/core/LinearProgress'; // add spiner
import Row from '../row';


export default class Section extends Component {
  state = { allCurrency: [] }
//state = { allCurrency: null }
    componentDidMount() {
        data(this.props.url)
            .then((data1) => {
                this.setState({
                    allCurrency: data1
                })
            });
    }

    componentDidUpdate(prevProps) { // оновлення компоненти при переключені сторінок
        if (prevProps.url !== this.props.url) {  //прописуємо умову краще всьго роботии через !==
            data(this.props.url)
                .then((data1) => {
                    this.setState({
                        allCurrency: data1
                    })
                });
        }
    }

    saveChange(allCurrency) {
        this.setState({
            allCurrency
        })
    }
    render() {
        const { allCurrency } = this.state;
        return (
            <table className='table-back'>
                <tbody>
                    <tr>
                        <th>Currency name</th>
                        <th>Price</th>
                        <th>Cod</th>
                    </tr>
                    {Array.isArray(allCurrency) ?
                        allCurrency.map((item, index) => {
                            return (
                                <Row key={index + 'r'} rowData={item}></Row>
                            )
                        }) : <LinearProgress />} 
                </tbody>

            </table>
        )
    }
}